package com.javagda23.kolekcje.mapa.zad1;

import java.util.*;

public class University {
    private Map<Long, Student> map = new TreeMap<>();

    public void dodajStudenta(long indeks, String imie, String nazwisko) {
        map.put(indeks, new Student(indeks, imie, nazwisko));
    }

    /**
     * Generuje index jako najwyższy + 1
     *
     * @param imie
     * @param nazwisko
     */
    public void dodajStudenta(String imie, String nazwisko) {
        List<Long> longList = new ArrayList<>(map.keySet());
        Long newIndex = longList.get(longList.size() - 1) + 1;

        map.put(newIndex, new Student(newIndex, imie, nazwisko));
        System.out.println("Dodaje na indeks : " + newIndex);
    }

    public void dodajStudentaDopokiJest(String imie, String nazwisko) {
        Random generator = new Random();
        Long wylosowanyIndex;
        do {
            wylosowanyIndex = (long) generator.nextInt(10000);
        } while (map.containsKey(wylosowanyIndex));

        map.put(wylosowanyIndex, new Student(wylosowanyIndex, imie, nazwisko));
        System.out.println("Dodaje na indeks : " + wylosowanyIndex);
    }

    public boolean containsStudent(Long index) {
        return map.containsKey(index);
    }

    public Student getStudent(long index) throws NoSuchStudentException {
        if (!containsStudent(index)) {
            throw new NoSuchStudentException();
        }
        return map.get(index);
    }

    public int studentsCount() {
        return map.size();
    }

    public void printAllStudents() {
        for (Student value : map.values()) {
            System.out.println(value);
        }
    }
}
