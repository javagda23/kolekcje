package com.javagda23.kolekcje.mapa.zad1;

public class NoSuchStudentException extends RuntimeException{
    public NoSuchStudentException() {
        super("Nie ma takiego studencika.");
    }
}
