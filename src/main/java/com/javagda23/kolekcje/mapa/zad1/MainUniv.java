package com.javagda23.kolekcje.mapa.zad1;

public class MainUniv {
    public static void main(String[] args) {
        University university = new University();

        university.dodajStudenta(101L, "a", "b");
        university.dodajStudenta(102L, "c", "z");
        university.dodajStudenta(103L, "g", "x");
        university.dodajStudenta(104L, "h", "w");

        university.getStudent(101); // znajdzie
        university.getStudent(101L);

        if(university.containsStudent(102L)){
            System.out.println("Mamy studenta 102L");
        }else{
            System.out.println("Nie ma studenta 102L");
        }

        System.out.println(university.getStudent(103L));

        university.printAllStudents();

        //
        try {
//            Student st = university.getStudent(101L);
            Student st = university.getStudent(105L);
            System.out.println("Znalazłem:" + st);
        } catch (NoSuchStudentException nsse) {
            System.out.println(nsse);
        }
    }
}
