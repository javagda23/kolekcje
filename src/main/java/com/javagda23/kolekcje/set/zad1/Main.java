package com.javagda23.kolekcje.set.zad1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
//        Set<Integer> set = new HashSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));
        Set<Integer> set = new TreeSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));

        printSizeAndElements(set);

        set.remove(10);
        set.remove(12);

        printSizeAndElements(set);
    }

    private static void printSizeAndElements(Set<Integer> set) {
        System.out.println("Rozmiar: " + set.size());
        System.out.println("Elementy:");
        for (Integer integer : set) {
            System.out.println(integer);
        }
        System.out.println();
    }
}
