package com.javagda23.kolekcje.set;

import com.javagda23.kolekcje.lista.zad5.Plec;
import com.javagda23.kolekcje.lista.zad5.Student;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
//        Zasad Liskov'a
//        List<Integer> list = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        Set<Student> studentHashSet = new HashSet<>();

        // elementy w secie nie są poukładane
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(5);
        set.add(5);
        set.add(null);
        set.add(null);
        System.out.println(set);// elementów: 6

        studentHashSet.add(new Student("1", "a", "b", Plec.KOBIETA));
        studentHashSet.add(new Student("2", "a", "b", Plec.KOBIETA));
        studentHashSet.add(new Student("3", "a", "b", Plec.KOBIETA));
        studentHashSet.add(new Student("4", "a", "b", Plec.KOBIETA));
        studentHashSet.add(new Student("5", "a", "b", Plec.KOBIETA));
        studentHashSet.add(new Student("6", "a", "b", Plec.KOBIETA));
        System.out.println(studentHashSet);

        Set<String> setString = new HashSet<>();

        List<Integer> ar = new ArrayList<>();
    }
}
