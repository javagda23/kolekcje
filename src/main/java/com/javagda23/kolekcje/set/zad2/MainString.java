package com.javagda23.kolekcje.set.zad2;

import java.util.*;

public class MainString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj tekst:");
        String linia = scanner.nextLine();

        System.out.println("Duplikaty:" + czyZawieraDuplikaty(linia));
    }

    public static boolean czyZawieraDuplikaty(String linia) {
        // czy mamy powtarzające się słowa/litery?
        String[] strings = linia.replaceAll(" ", "").replaceAll("[.]", "").replaceAll(",", "").split("");
        List<String> slowa = new ArrayList<>(Arrays.asList(strings));
        Set<String> bezDuplikatow = new HashSet<>(slowa);

        return slowa.size() == bezDuplikatow.size();
    }
}
