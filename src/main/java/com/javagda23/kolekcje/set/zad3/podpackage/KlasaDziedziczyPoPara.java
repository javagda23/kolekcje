package com.javagda23.kolekcje.set.zad3.podpackage;

import com.javagda23.kolekcje.set.zad3.Para;

public class KlasaDziedziczyPoPara extends Para {
    public KlasaDziedziczyPoPara() {
        super();
        super.metodaProtected();
//        super.metodaBezModyfikatora(); // < try me
//        metodaPublic(1, 2);
    }
}
