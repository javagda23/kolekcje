package com.javagda23.kolekcje.set.zad3;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Para> set = new HashSet<>();

        Para p = new Para(5, 5);

        set.add(new Para(1, 2));
        set.add(new Para(2, 1));
        set.add(new Para(1, 1));
        set.add(new Para(1, 2));

        System.out.println(set);

        LocalDateTime.now();

    }
}
