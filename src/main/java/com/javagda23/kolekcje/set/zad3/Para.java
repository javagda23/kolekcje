package com.javagda23.kolekcje.set.zad3;

import lombok.*;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class Para {

    @ToString.Include
    @EqualsAndHashCode.Include
    private int a;

    @ToString.Exclude
    @EqualsAndHashCode.Include
    private int b;

//    public int metodaPublic(int a, int b){
//
//    }

//    public void metodaPublic(int b, int a){
//
//    }

    protected void metodaProtected(){

    }

    void metodaBezModyfikatora(){

    }
}
