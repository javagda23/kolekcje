package com.javagda23.kolekcje.lista.zad5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student("1", "a", "b", Plec.KOBIETA));
        studentList.add(new Student("2", "d", "c", Plec.MEZCZYZNA));
        studentList.add(new Student("3", "e", "f", Plec.MEZCZYZNA));
        studentList.add(new Student("4", "h", "g", null));
        studentList.add(new Student("5", "i", "j", Plec.MEZCZYZNA));
        studentList.add(new Student("6", "l", "k", Plec.MEZCZYZNA));

        // a)
        System.out.println(studentList); // [ ]

        // b)
        System.out.println();
        for (Student student : studentList) {
            System.out.println(student);
        }

        // c)
        System.out.println();
        for (Student student : studentList) {
            if (student.getPlec() == Plec.KOBIETA) {
                System.out.println(student);
            }
        }

        // d)
        System.out.println();
        for (Student student : studentList) {
//            if (student.getPlec() == Plec.MEZCZYZNA) {        // zawsze bezpieczne
//            if (student.getPlec().equals(Plec.MEZCZYZNA)) {   // podatna na błąd. metoda getPlec może zwrócić null,
                                                                // a tym samym otrzymamy null pointer exception
                                                                // odwrócenie tego (linia niżej) zapobiega temu problemowi
            if (Plec.MEZCZYZNA.equals(student.getPlec())) {
                System.out.println(student);
            }
        }

        // e) wypisanie tylko indeksów
        System.out.println();
        for (Student student : studentList) {
            System.out.println(student.getNrIndeksu());
        }
    }
}
