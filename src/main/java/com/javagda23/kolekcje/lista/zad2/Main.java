package com.javagda23.kolekcje.lista.zad2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            list.add(new Random().nextInt(100));
        }

        double suma = 0;
        for (Integer integer : list) {
            suma += integer;
        }
        System.out.println("Suma: " + suma);
        System.out.println("Średnia: " + (suma / list.size())); // int / int = int
        // double / int = double

        // tworzenie kopii kolekcji
        // w wyniku kopiowania otrzymujemy drugą listę z jednakowymi elementami
        List<Integer> kopia = new ArrayList<>(list);

//        List<Integer> kopia = list; // to nie jest kopiowanie listy
        // zadeklarowaliśmy drugą zmienną tego samego typu która będzie przechowywać DOKŁADNIE TĄ SAMĄ LISTĘ

        Collections.sort(list);

        System.out.println(list);

        double mediana;
        if (list.size() % 2 == 0) { // parzysta ilość elementów
            mediana = ((list.get(list.size() / 2 - 1)) + (list.get(list.size() / 2))) / 2.0; // 10 / 2 = 5 ==> 6 element
        } else { // nieparzysta ilość elementów
            mediana = list.get(list.size() / 2); // 9 / 2 = 4 ==> 5 element
        }

        System.out.println("Mediana: " + mediana);

        // znalezienie największy / najmniejszy
        int min = kopia.get(0), pozycjaMin = 0;
        int max = kopia.get(0), pozycjaMax = 0;

        for (int i = 1; i < kopia.size(); i++) {
            if (max < kopia.get(i)) {
                // znaleźliśmy nowe maximum
                max = kopia.get(i);
                pozycjaMax = i;
            }
            if (min > kopia.get(i)) {
                // nowe minimum
                min = kopia.get(i);
                pozycjaMin = i;
            }
        }
        System.out.println("Minimum: " + min + " znaleziony na pozycji: " + pozycjaMin);
        System.out.println("Maximum: " + max + " znaleziony na pozycji: " + pozycjaMax);

        /// to samo metodą indexOf();
        int pozMinIndexOf = kopia.indexOf(min);
        int pozMaxIndexOf = kopia.indexOf(max);
        System.out.println("Minimum: " + min + " znaleziony na pozycji: " + pozMinIndexOf);
        System.out.println("Maximum: " + max + " znaleziony na pozycji: " + pozMaxIndexOf);
    }
}
