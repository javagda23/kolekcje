package com.javagda23.kolekcje.lista.zad4;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Dziennik dziennik = new Dziennik();

        Student marek = new Student("123123", "a", "b");

        dziennik.dodajStudenta(marek);
        dziennik.dodajStudenta(marek);

//        dziennik.usunStudenta(marek);

        dziennik.usunStudenta("123123");

        Optional<Student> optionalStudent = dziennik.zwrocStudenta("123123");
        if (optionalStudent.isPresent()) {
            Student student = optionalStudent.get();

            System.out.println("Ziomek: " + student.getNazwisko());
        } else {
            System.out.println("Nie ma gościa.");
        }

        Optional<Double> srednia = dziennik.podajSredniaStudenta("123123");
    }
}
