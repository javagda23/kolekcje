package com.javagda23.kolekcje.lista.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Random generator = new Random();

        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            integers.add(generator.nextInt(201) - 100); // przedział +/- 100
        }

        for (int i = 0; i < 5; i++) {
            System.out.println("Podaj liczbę:");
            integers.add(scanner.nextInt());
        }

        System.out.println(integers);

        int min = -500;
        int max = 1000;

        int dlugoscPrzedzialu = max - min;

        double wynik = (Math.random() * dlugoscPrzedzialu) - min; //0-1.0

        System.out.println(String.format("%.2f", wynik));
    }
}
