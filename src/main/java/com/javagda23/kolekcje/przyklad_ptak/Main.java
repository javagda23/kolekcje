package com.javagda23.kolekcje.przyklad_ptak;

public class Main {
    public static void main(String[] args) {
        Kukulka k = new Kukulka();
        k.spiewaj(); // ku ku

        Ptak p = new Kukulka();
        p.spiewaj(); //

        Ptak[] ptaks = new Ptak[3];
        ptaks[0] = new Sowa();
        ptaks[1] = new Kukulka();
        ptaks[2] = new Sokol();

        for (Ptak ptak : ptaks) {
            ptak.spiewaj();
        }

    }
}
